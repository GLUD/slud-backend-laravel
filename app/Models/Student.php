<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Student extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    protected $fillable = ['name', 'lastName', 'email', 'password', 'api_token', 'remember_token'];

    protected $hidden = ['password', 'remember_token', 'updated_at'];
    //Relationship
    public function loans(){
        return $this->hasMany(Loan::class);
    }
}
