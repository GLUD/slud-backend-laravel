<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Book extends Model
{
    use HasFactory;
    //Relationship
    public function editorial(){
        return $this->belongsTo(Editorial::class);
    }

    public function author(){
        return $this->belongsTo(Author::Class);
    }
    
    public function loans(){
        return $this->hasMany(Loan::class);
    }

    //SCOPES
    public function scopeWhereHasEditorialWithAuthor($query, $editorial){
        $query->WhereHas('editorial', function (Builder $query) use ($editorial){
            $query->where('name', $editorial);
        })->with(['author' => function($query){
            $query->selectRaw('id, CONCAT(name, " ", lastName) AS fullName');
        }]);
    }
}
