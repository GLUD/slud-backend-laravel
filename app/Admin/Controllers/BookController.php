<?php

namespace App\Admin\Controllers;

use App\Models\Book;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Author;
use App\Models\Editorial;

class BookController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Book';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Book());

        $grid->column('id', __('Id'));
        $grid->column('name', __('Nombre'));
        $grid->column('author', __('Nombre del autor'))->display(function($author){
            return $author['name'].' '.$author['lastName'];
        });
        $grid->column('editorial.name', __('Editorial'));
        $grid->column('releaseDate', __('Fecha Lanzamiento'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Book::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('author_id', __('Author id'));
        $show->field('editorial_id', __('Editorial id'));
        $show->field('releaseDate', __('ReleaseDate'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Book());
        $author = Author::selectRaw('id, CONCAT(name, " ", lastName) AS fullName')->pluck('fullName', 'id');;
        $editorial = Editorial::pluck('name', 'id');

        $form->text('name', __('Nombre'));
        $form->select('author_id', __('Autor'))->options($author);
        $form->select('editorial_id', __('Editorial'))->options($editorial);
        $form->date('releaseDate', __('Fecha de Lanzamiento'))->default(date('Y-m-d'));

        return $form;
    }
}
