<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Controller Invokable
Route::get('/books-editorial/{editorial}', BooksController::class);

//Basic Controller
Route::get('/find-books-author/{autor}', 'ExampleController@authorWithBooks');
Route::get('/find-books-editorial/{editorial}', 'ExampleController@editorialWithBooks');
Route::post('/find-books-editorial-post', 'ExampleController@editorialWithBooksPost');
Route::post('/find-books-author-post', 'ExampleController@authorWithBooksPost');
Route::post('/find-books-no-loans-author-post', 'ExampleController@authorWithNotLoanedBooksPost');
Route::get('/find-books-no-loans', 'ExampleController@noLoanBooks');
Route::post('/find-books-editorial-author', 'ExampleController@editorialWithBooksAndAuthor');

//Resource
Route::resource('/authors', AuthorsController::class);

//Auth
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthTokenController@login');
    Route::post('signup', 'AuthTokenController@signUp');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthTokenController@logout');
    });
});

Route::get('/student', StudentController::class);